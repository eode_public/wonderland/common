**This is the home page of [this documentation](http://wonderland.eode.studio/docs/common-_introduction.html)**

## Introduction

### Version
> Released, actively maintained


This module groups all common and utils scripts and other basics extern modules.

### Main namespace
```C#
using EODE.Wonderland;
```

### Requirements
- .NET 4.x

### External packages included
- Async Await Support (see [Async](http://wonderland.eode.studio/docs/common-Async.html))